import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';

import '../constants/theme.dart';

class SectionTitle extends StatelessWidget {
  final String? title;
  final String? subtitle;

  const SectionTitle({
    Key? key, required this.title, this.subtitle = "",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title!,
              style: GoogleFonts.poppins(
                fontSize: 24,
                color: blackPrimary
              )),
          Text(subtitle!,
              style: GoogleFonts.poppins(
                fontSize: 16,
                color: greyPrimary,
              )),
        ],
      ),
    );
  }
}