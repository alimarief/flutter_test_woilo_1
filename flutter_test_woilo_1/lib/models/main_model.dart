class MainModel {
  MainModel({
    required this.result,
    required this.message,
    required this.data,
  });
  late final int result;
  late final String message;
  late final List<MainDataModel> data;
  
  MainModel.fromJson(Map<String, dynamic> json){
    result = json['result'];
    message = json['message'];
    data = List.from(json['data']).map((e)=>MainDataModel.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['result'] = result;
    _data['message'] = message;
    _data['data'] = data.map((e)=>e.toJson()).toList();
    return _data;
  }
}

class MainDataModel {
  MainDataModel({
    required this.displayName,
    required this.displayPicture,
    required this.contentPhoto,
    required this.caption,
    required this.timeStamp,
  });
  late final String displayName;
  late final String displayPicture;
  late final String contentPhoto;
  late final String caption;
  late final String timeStamp;
  
  MainDataModel.fromJson(Map<String, dynamic> json){
    displayName = json['display_name'];
    displayPicture = json['display_picture'];
    contentPhoto = json['content_photo'];
    caption = json['caption'];
    timeStamp = json['time_stamp'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['display_name'] = displayName;
    _data['display_picture'] = displayPicture;
    _data['content_photo'] = contentPhoto;
    _data['caption'] = caption;
    _data['time_stamp'] = timeStamp;
    return _data;
  }
}