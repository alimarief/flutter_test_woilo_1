import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_test_woilo_1/models/auth_model.dart';
import 'package:flutter_test_woilo_1/services/auth_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthService _authService = AuthService();
  bool loggedIn = false;


  AuthBloc() : super(AuthInitial()) {

    on<LoginEvent>((event, emit) async {
      emit(LoginLoadingState());

      final prefs = await SharedPreferences.getInstance();

      AuthModel apiResult = await _authService.login(event.username, event.password);
      prefs.setString('username', event.username);
      prefs.setString('password', event.password);

      if(apiResult.result == 1) {
        emit(LoginSuccessState());
        loggedIn = true;
      } else {
        loggedIn = false;
        emit(LoginErrorState(errorMessage: apiResult.message));
      }


    });
  }
}
