part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();
  
  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class LoginSuccessState extends AuthState{

}

class LoginLoadingState extends AuthState{

}

class LoginErrorState extends AuthState{
  final String errorMessage;

  LoginErrorState({required this.errorMessage});
  
  @override
  List<Object> get props => [errorMessage];

}
