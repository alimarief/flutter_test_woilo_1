import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_test_woilo_1/models/main_model.dart';
import 'package:flutter_test_woilo_1/services/main_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  MainService _mainService = MainService();

  HomeBloc() : super(HomeInitial()) {
    on<FetchHomeEvent>((event, emit) async {
      emit(HomeLoadingState());
      final prefs = await SharedPreferences.getInstance();

      final String? username = prefs.getString('username');
      final String? password = prefs.getString('password');

      _mainService.getDataMain(username, password);

      print("SHARED");
      print("SHARED U = $username");
      print("SHARED P = $password");

      MainModel apiResult = await _mainService.getDataMain(username, password);

      print("DATA LENG = ${apiResult.data.length}");

      if (apiResult.result == 1) {
        emit(HomeLoadedState(data: apiResult.data));
      } else {
        emit(HomeErrorState(errorMessage: apiResult.message));
      }
    });
  }
}
