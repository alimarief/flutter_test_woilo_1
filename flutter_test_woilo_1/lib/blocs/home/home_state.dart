part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();
  
  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}

class HomeLoadingState extends HomeState{}

class HomeLoadedState extends HomeState{
  final List<MainDataModel> data;

  HomeLoadedState({this.data = const []});
  
  @override
  List<Object> get props => [data];
}

class HomeErrorState extends HomeState{
  final String errorMessage;

  HomeErrorState({this.errorMessage = ""});
  
  @override
  List<Object> get props => [errorMessage];
}