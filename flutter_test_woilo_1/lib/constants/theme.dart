import 'package:flutter/material.dart';

Color purpleDarker = Color(0XFF2536A7); 
Color purplePrimary = Color(0XFF475AD7); 
Color purpleLight = Color(0XFF8A96E5); 
Color purpleLighter = Color(0XFFEEF0FB); 
Color blackDarker = Color(0XFF22242F);
Color blackPrimary = Color(0XFF333647);
Color blackLight = Color(0XFF44485F);
Color blackLighter = Color(0XFF555A77);
Color greyDarker = Color(0XFF666C8E);
Color greyPrimary = Color(0XFF7C82A1);
Color greyLight = Color(0XFFACAFC3);
Color greyLighter = Color(0XFFF3F4F6);

