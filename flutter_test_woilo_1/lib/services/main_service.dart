import 'dart:developer';

import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:flutter_test_woilo_1/constants/constant.dart';
import 'package:http/http.dart' as http;

import '../models/main_model.dart';

class MainService {
  late Response response;
  late Dio _dio = Dio();

  Future<MainModel> getDataMain(username, password) async {
    // String username = "user";
    // String password =
    //     "04f8996da763b7a969b1028ee3007569eaf3a635486ddab211d512c85b9df8fb";

    // var convertToBytes = utf8.encode("user");
    // var convertToSha26 = sha256.convert(convertToBytes);

    // print(convertToSha26);

    var params = jsonEncode(<String, String>{
      'user_name': username,
      'password': password,
    });

    Response response = await _dio.post(
      homeUrl,
      data: params,
    );

    try {
      if (response.statusCode == 200) {
        log(response.data);
        return MainModel.fromJson(jsonDecode(response.data));
      }
      return MainModel.fromJson(jsonDecode(response.data));
    } catch (e) {
      print(e);
      return MainModel.fromJson(jsonDecode(response.data));
    }
  }
}
