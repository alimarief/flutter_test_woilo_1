import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:flutter_test_woilo_1/constants/constant.dart';
import 'package:flutter_test_woilo_1/models/auth_model.dart';
import 'package:http/http.dart' as http;

class AuthService {
  late Response response;
  late Dio _dio = Dio();

  Future<AuthModel> login(username, password) async {
    var params = jsonEncode(<String, String>{
      'user_name': username,
      'password': password,
    });

    Response response = await _dio.post(
      loginUrl,
      data: params,
    );

    try {
      if (response.statusCode == 200) {
        return AuthModel.fromJson(jsonDecode(response.data));
      }

      return AuthModel.fromJson(jsonDecode(response.data));
    } on DioError catch (e) {
      print(e);
      return AuthModel.fromJson(jsonDecode(response.data));
    }

  
  }
}
