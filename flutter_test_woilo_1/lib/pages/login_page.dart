import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test_woilo_1/pages/home_page.dart';
import 'package:flutter_test_woilo_1/services/auth_service.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';

import '../blocs/auth/auth_bloc.dart';
import '../constants/theme.dart';
import '../widgets/section_tilte.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  late bool isHidden = true;
  AuthService _authService = AuthService();

  @override
  Widget build(BuildContext context) {
    AuthBloc authBloc = context.read<AuthBloc>();

    return BlocListener<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is LoginSuccessState) {
          Get.to(() => HomePage());
        }
        if (state is LoginErrorState) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              backgroundColor: Colors.redAccent,
              content: Text(state.errorMessage),
            ),
          );
        }
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Form(
              autovalidateMode: AutovalidateMode.always,
              key: formkey,
              child: Column(
                children: [
                  const SectionTitle(
                    title: "Hello 👋",
                    subtitle: "Welcome back to woilo",
                  ),
                  const SizedBox(height: 32),
                  Container(
                    margin: const EdgeInsets.only(left: 20, right: 20),
                    child: Column(
                      children: [
                        TextFormField(
                          controller: _usernameController,
                          decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.person,
                                color: purplePrimary,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: purplePrimary,
                                  width: 1.5,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: purplePrimary,
                                  width: 1.5,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: purplePrimary,
                                  width: 1.5,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: purplePrimary,
                                  width: 1.5,
                                ),
                              ),
                              hintText: 'Username'),
                          validator: MultiValidator([
                            RequiredValidator(errorText: "* Required"),
                          ]),
                        ),
                        const SizedBox(height: 16),
                        TextFormField(
                          obscureText: isHidden,
                          controller: _passwordController,
                          decoration: InputDecoration(
                              suffixIcon: IconButton(
                                icon: FaIcon(
                                    (isHidden)
                                        ? FontAwesomeIcons.eye
                                        : FontAwesomeIcons.eyeSlash,
                                    size: 20,
                                    color: purplePrimary),
                                // Icon(
                                //   (isHidden) ? Icons.f:
                                //   Icons.remove_red_eye_outlined),
                                onPressed: () {
                                  setState(() {
                                    isHidden = !isHidden;
                                  });
                                },
                              ),
                              prefixIcon: Icon(
                                Icons.lock_outline_rounded,
                                color: purplePrimary,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: purplePrimary,
                                  width: 1.5,
                                ),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: purplePrimary,
                                  width: 1.5,
                                ),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: purplePrimary,
                                  width: 1.5,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  color: purplePrimary,
                                  width: 1.5,
                                ),
                              ),
                              hintText: 'Password'),
                          validator: MultiValidator(
                            [
                              // RequiredValidator(errorText: "* Required"),
                              MinLengthValidator(4,
                                  errorText:
                                      "Password should be atleast 4 characters"),
                              MaxLengthValidator(15,
                                  errorText:
                                      "Password should not be greater than 4 characters")
                            ],
                          ),
                        ),
                        SizedBox(height: 16),
                        Container(
                          alignment: Alignment.centerRight,
                          margin: EdgeInsets.only(bottom: 24),
                          child: GestureDetector(
                            onTap: () {},
                            child: Text(
                              "Forgot Password?",
                              style: GoogleFonts.poppins(
                                color: greyPrimary,
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          height: 50,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: purplePrimary,
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: purplePrimary,
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12),
                              ),
                            ),
                            child: Text(
                              "Sign In",
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            onPressed: () {
                              print(_usernameController.text);
                              print(_passwordController.text);

                              var convertToBytes =
                                  utf8.encode(_passwordController.text);
                              var convertToSha26 =
                                  sha256.convert(convertToBytes);

                              print(convertToSha26.toString());

                              authBloc.add(
                                LoginEvent(
                                  username: _usernameController.text,
                                  password: convertToSha26.toString(),
                                ),
                              );
                            },
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        RichText(
                          text: TextSpan(children: [
                            TextSpan(
                              text: "Don't have an account? ",
                              style: GoogleFonts.poppins(
                                color: blackLighter,
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            TextSpan(
                              text: "Sign Up",
                              style: GoogleFonts.poppins(
                                color: purplePrimary,
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ]),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
